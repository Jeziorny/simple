# Free uber-like app

You can post a ride with start and end address and someone can join you in journey.

## Use Cases
### UC1
As a user I want to inform others about where I am going to. For simplicity accept only start and end address, without intermediate points. Example `POST` ride data:
```json
{
    'start_coords': {
        'formatted_address': 'address A',
        'lat': 2.1,
        'lng': 2.2,
        'datetime': '2022-07-05T08:46:29.895468Z'
    },
    'end_coords': {
        'formatted_address': 'address B',
        'lat': 2.3,
        'lng': 2.4,
        'datetime': '2022-07-05T08:46:29.895468Z'
    },
    'number_of_free_seats': 69,
    'car_brand': 'Mercedes',
    'car_model': 'T 620',
    'car_color': 'Green',
    'car_register_number': 'REG2137'
}
```

### UC2
As a user I want to join a ride. Example of `PUT` data, to join a ride:
```json
{'number_of_free_seats': 70}
```

### UC3
As a user I want to have my recent car data to be cached to avoid typing in every time when posting a ride. Example of `POST` a car
```json
{
    'brand': 'VW',
    'model': 'Golf V',
    'number_of_seats': 4,
}
```
Example of get recent car data:
```json
[{'brand': 'A',
  'model': 'T 620',
  'number_of_seats': 69,
  'color': '',
  'register_number': '',
  'owner': 'kamij98@gmail.com'},
 {'brand': 'B',
  'model': 'T 620',
  'number_of_seats': 69,
  'color': '',
  'register_number': '',
  'owner': 'kamij98@gmail.com'},
 {'brand': 'C',
  'model': 'T 620',
  'number_of_seats': 69,
  'color': '',
  'register_number': '',
  'owner': 'kamij98@gmail.com'},
 {'brand': 'D',
  'model': 'T 620',
  'number_of_seats': 69,
  'color': '',
  'register_number': '',
  'owner': 'kamij98@gmail.com'},
 {'brand': 'E',
  'model': 'T 620',
  'number_of_seats': 69,
  'color': '',
  'register_number': '',
  'owner': 'kamij98@gmail.com'}]
```

Database stores up to 5 recent cars. Every new post will replace oldest car from database.

### UC4
As a user I want to change my password. Example of changing password is in `test_user.py`


## Models
### Coords
Holds base data about address. It could be a start address or end address.

### Ride
Holds data about:
- start and end address,
- number of free seats,
- informations describing a car,
- owner of a ride.

### Car
Car model is used only for caching recent user entries purposes. Car used in a ride is described in Ride model. Holds car owner, last modified (for removing oldest one if recent car number is >= 5) and information used for recognizing.

### User
Users will be identified by email.
