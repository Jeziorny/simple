from django.urls import path

from car import views

urlpatterns = [
    path("", views.CarListCreatetAPIView.as_view(), name="car-list"),
]
