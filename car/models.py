from django.db import models
from django.contrib.auth import get_user_model

_RECENT_CAR_LIMIT = 3

User = get_user_model()

class CarManager(models.Manager):
    def create_removing_oldest_if_more_than_limit(self, **kwargs):
        qs = super().get_queryset().filter(owner=kwargs['owner']).order_by('-last_modified')
        if len(qs) >= _RECENT_CAR_LIMIT:
            qs.last().delete()
        return super().create(**kwargs)


class Car(models.Model):
    objects = CarManager()

    brand = models.CharField(max_length=128)
    model = models.CharField(max_length=128)
    number_of_seats = models.IntegerField()
    color = models.CharField(max_length=32, blank=True)
    register_number = models.CharField(max_length=16, blank=True)
    last_modified = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)


