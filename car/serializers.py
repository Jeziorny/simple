from rest_framework import serializers

from car.models import Car


class CarSerializer(serializers.ModelSerializer):
    owner = serializers.CharField(read_only=True)

    class Meta:
        model = Car
        fields = [
            'brand',
            'model',
            'number_of_seats',
            'color',
            'register_number',

            'owner'
        ]

    def create(self, validated_data):
        return Car.objects.create_removing_oldest_if_more_than_limit(**validated_data)
