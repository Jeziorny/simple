FROM python:3.10-alpine

WORKDIR /code

COPY ride /code/ride
COPY simple /simple/simple
COPY requirements.txt /code
COPY manage.py /code

COPY . /code

RUN pip install -r requirements.txt
