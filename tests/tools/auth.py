from rest_framework.test import APITestCase

from django.urls import reverse


def set_credentials(client: APITestCase.client_class, username: str, password: str) -> str:
    response = client.post(
        reverse('token_obtain_pair'),
        data={"email": username, "password": password}
    )
    access_key = response.json().get('access')
    assert access_key, f"Error during setting creds: {response.json()}"
    client.credentials(HTTP_AUTHORIZATION="Bearer " + access_key)
    return response.json()['pk']
