from http import HTTPStatus

from django.urls import reverse
from django.contrib.auth import get_user_model

from rest_framework.test import APIClient

import pytest

from tests.tools.auth import set_credentials

_EMAIL = 'test@gmail.com'
_PASSWORD = 'bombiszczak123'
_FIRST_NAME = 'John'
_LAST_NAME = 'Doe'
_DUMMY_REGISTER_DATA = {
    'email': _EMAIL,
    'password': _PASSWORD,
    'password2': _PASSWORD,
    'first_name': _FIRST_NAME,
    'last_name': _LAST_NAME,
}

User = get_user_model()


@pytest.fixture
def apiclient():
    return APIClient()


@pytest.mark.django_db
def test_change_password(apiclient):
    new_password = "HasloJakMaslo123"
    change_password_data = {
        'old_password': _PASSWORD,
        'password': new_password,
        'password2': new_password,
    }

    register_response = apiclient.post(reverse('register'), data=_DUMMY_REGISTER_DATA)
    user_pk = set_credentials(apiclient, _EMAIL, _PASSWORD)
    change_password_response = apiclient.put(
        reverse('change_password',  kwargs={'pk': user_pk}), data=change_password_data
    )
    _ = set_credentials(apiclient, _EMAIL, new_password)

    assert register_response.status_code == HTTPStatus.CREATED
    assert change_password_response.status_code == HTTPStatus.OK


@pytest.mark.django_db
def test_delete_user(apiclient):
    register_response = apiclient.post(reverse('register'), data=_DUMMY_REGISTER_DATA)
    set_credentials(apiclient, _EMAIL, _PASSWORD)
    delete_response = apiclient.delete(reverse('delete_user'))
    token_obtain_response = apiclient.post(reverse('token_obtain_pair'), data={"email": _EMAIL, "password": _PASSWORD})

    assert register_response.status_code == HTTPStatus.CREATED
    assert delete_response.status_code == HTTPStatus.NO_CONTENT
    assert token_obtain_response.status_code == HTTPStatus.UNAUTHORIZED
