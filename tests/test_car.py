from http import HTTPStatus

from django.urls import reverse
from django.contrib.auth import get_user_model

from rest_framework.test import APIClient

from tests.tools.auth import set_credentials
from car.models import Car, _RECENT_CAR_LIMIT

import pytest

_EMAIL = 'test'
_PASSWORD = 'password'
_DUMMY_CAR_DATA = {
    'brand': 'VW',
    'model': 'Golf V',
    'number_of_seats': 4,
}

User = get_user_model()

@pytest.fixture
def apiclient():
    return APIClient()


@pytest.mark.django_db
def test_posting_car_overrides_oldest_entry_if_limit_exceeds(apiclient):
    user = User.objects.create_user(email=_EMAIL, password=_PASSWORD)
    for _ in range(_RECENT_CAR_LIMIT):
        Car.objects.create(**_DUMMY_CAR_DATA, owner=user)
    set_credentials(apiclient, _EMAIL, _PASSWORD)

    post_car_response = apiclient.post(reverse('car-list'), data=_DUMMY_CAR_DATA)
    get_car_response = apiclient.get(reverse('car-list'))

    assert post_car_response.status_code == HTTPStatus.CREATED
    assert len(get_car_response.json()) == _RECENT_CAR_LIMIT


@pytest.mark.django_db
def test_get_cars_returns_only_user_owned_cars(apiclient):
    other_user = User.objects.create_user(email='other_user', password='other_password')
    Car.objects.create(**_DUMMY_CAR_DATA, owner=other_user)
    tested_user = User.objects.create_user(email=_EMAIL, password=_PASSWORD)
    Car.objects.create(**_DUMMY_CAR_DATA, owner=tested_user)
    set_credentials(apiclient, _EMAIL, _PASSWORD)
    expected_data = [{
        'color': '',
        'register_number': '',
        'owner': _EMAIL,
        **_DUMMY_CAR_DATA,
    }]

    get_car_list_response = apiclient.get(reverse('car-list'))

    assert get_car_list_response.json() == expected_data
