from datetime import datetime
from django.utils import timezone
from http import HTTPStatus

from django.urls import reverse
from django.contrib.auth import get_user_model

from rest_framework.test import APIClient

from tests.tools.auth import set_credentials

from ride.models import Coords, Ride
from car.models import Car
import pytest


_EMAIL = 'test'
_PASSWORD = 'password'
_DUMMY_RIDE_DATA = {
    'start_coords': {
        'id': 25,
        'formatted_address': 'address A',
        'lat': 2.1,
        'lng': 2.2,
        'datetime': '2022-07-05T08:46:29.895468Z'
    },
    'end_coords': {
        'id': 26,
        'formatted_address': 'address B',
        'lat': 2.3,
        'lng': 2.4,
        'datetime': '2022-07-05T08:46:29.895468Z'
    },
    'number_of_free_seats': 69,
    'car_brand': 'Mercedes',
    'car_model': 'T 620',
    'car_color': 'Green',
    'car_register_number': 'FOO'
}

User = get_user_model()


@pytest.fixture
def apiclient():
    return APIClient()


@pytest.mark.django_db
@pytest.mark.parametrize('update_param', [
    'car_brand',
    'car_model',
    'car_color',
    'car_register_number',
])
def test_updating_fields_other_than_number_of_free_seats_has_no_effect(apiclient, update_param):
    User.objects.create_user(email=_EMAIL, password=_PASSWORD)
    set_credentials(apiclient, _EMAIL, _PASSWORD)
    number_of_free_seats_update_param = {'number_of_free_seats': 69}

    post_ride_response = apiclient.post(
        reverse('ride-create'), data=_DUMMY_RIDE_DATA, format='json'
    )
    update_ride_response = apiclient.put(
        reverse('ride-update', kwargs={'pk': 1}), data={**{update_param: 'new'}, **number_of_free_seats_update_param}
    )

    tested_ride = Ride.objects.all().first()
    assert post_ride_response.status_code == HTTPStatus.CREATED
    assert update_ride_response.status_code == HTTPStatus.OK
    assert getattr(tested_ride, update_param) == _DUMMY_RIDE_DATA[update_param]


@pytest.mark.django_db
def test_updating_number_of_free_seats(apiclient):
    User.objects.create_user(email=_EMAIL, password=_PASSWORD)
    set_credentials(apiclient, _EMAIL, _PASSWORD)
    number_of_free_seats_update_param = {'number_of_free_seats': 70}

    post_ride_response = apiclient.post(
        reverse('ride-create'), data=_DUMMY_RIDE_DATA, format='json'
    )
    update_ride_response = apiclient.put(
        reverse('ride-update', kwargs={'pk': 1}), data=number_of_free_seats_update_param
    )

    updated_ride = Ride.objects.all().first()
    assert post_ride_response.status_code == HTTPStatus.CREATED
    assert update_ride_response.status_code == HTTPStatus.OK
    assert updated_ride.number_of_free_seats == 70


@pytest.mark.django_db
def test_delete_ride_not_allowed_for_other_than_owner(apiclient):
    other_user = User.objects.create_user(email='other_user', password='other_password')
    start_coords = Coords.objects.create(**_DUMMY_RIDE_DATA['start_coords'])
    end_coords = Coords.objects.create(**_DUMMY_RIDE_DATA['end_coords'])
    Ride.objects.create(
        **{
            **_DUMMY_RIDE_DATA,
            'start_coords': start_coords,
            'end_coords': end_coords,
        },
        owner=other_user
    )
    User.objects.create_user(email=_EMAIL, password=_PASSWORD)
    set_credentials(apiclient, _EMAIL, _PASSWORD)

    delete_ride_response = apiclient.delete(
        reverse('ride-delete', kwargs={'pk': 1}),
    )
    
    assert delete_ride_response.status_code == HTTPStatus.FORBIDDEN

