from django.contrib.auth import get_user_model
from authentication.serializers import (
    RegisterSerializer, DestroyUserSerializer, ChangeUserPasswordSerializer,
    PkTokenObtainPairSerializer
)
from rest_framework import generics, permissions
from rest_framework_simplejwt.views import TokenObtainPairView

User = get_user_model()


class PkTokenObtainPairView(TokenObtainPairView):
    serializer_class = PkTokenObtainPairSerializer


class RegisterView(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = RegisterSerializer
    permission_classes = [permissions.AllowAny]


class ChangeUserPasswordView(generics.UpdateAPIView):
    queryset = User.objects.all()
    serializer_class = ChangeUserPasswordSerializer
    permission_classes = [permissions.IsAuthenticated]


class DeleteView(generics.DestroyAPIView):
    queryset = User.objects.all()
    serializer_class = DestroyUserSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self):
        return self.queryset.get(email=self.request.user.email)
