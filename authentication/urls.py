from django.urls import path
from rest_framework_simplejwt.views import (
    TokenRefreshView,
    TokenVerifyView,
)
from authentication.views import (
    RegisterView, DeleteView, ChangeUserPasswordView, PkTokenObtainPairView
)


urlpatterns = [
    path('login/', PkTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('login/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('login/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('register/', RegisterView.as_view(), name='register'),
    path('change_password/<int:pk>/', ChangeUserPasswordView.as_view(), name='change_password'),
    path('delete/', DeleteView.as_view(), name='delete_user'),
]
