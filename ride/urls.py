from django.urls import path

from ride import views

urlpatterns = [
    path("", views.RideListAPIView.as_view(), name="ride-list"),
    path("create/", views.RideCreateAPIView.as_view(), name="ride-create"),
    path("update/<int:pk>", views.RideUpdateAPIView.as_view(), name="ride-update"),
    path("delete/<int:pk>", views.RideDestroyAPIView.as_view(), name="ride-delete")
]
