from rest_framework import serializers
from car.models import Car

from ride.models import Ride, Coords

class CoordsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coords
        fields = '__all__'


class PostRideSerializer(serializers.ModelSerializer):
    start_coords = CoordsSerializer()
    end_coords = CoordsSerializer()

    class Meta:
        model = Ride
        fields = [
            'start_coords',
            'end_coords',

            'number_of_free_seats',
            'car_brand',
            'car_model',
            'car_color',
            'car_register_number',
        ]

    def create(self, validated_data):
        start_coords_data = validated_data.pop('start_coords')
        end_coords_data = validated_data.pop('end_coords')
        start_coords = Coords.objects.create(**start_coords_data)
        end_coords = Coords.objects.create(**end_coords_data)
        ride = Ride.objects.create(
            start_coords=start_coords,
            end_coords=end_coords,
            **validated_data,
        )
        return ride


class ListRideSerializer(serializers.ModelSerializer):
    start_coords = CoordsSerializer()
    end_coords = CoordsSerializer()
    update_url = serializers.HyperlinkedIdentityField(view_name='ride-update')
    delete_url = serializers.HyperlinkedIdentityField(view_name='ride-delete')


    class Meta:
        model = Ride
        fields = [
            'start_coords',
            'end_coords',

            'update_url',
            'delete_url',

            'number_of_free_seats',
            'car_brand',
            'car_model',
            'car_color',
            'car_register_number',
        ]


class UpdateRideSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ride
        fields = ['number_of_free_seats']


class DestroyRideSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ride
