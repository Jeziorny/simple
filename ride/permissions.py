from email import message
from rest_framework import permissions

class IsOwner(permissions.BasePermission):
    message = 'You are not allowed to destroy this ride'

    def has_object_permission(self, request, view, obj):
        return obj.owner == request.user