from django.db import models
from authentication.views import User


class Coords(models.Model):
    formatted_address = models.CharField(max_length=128)
    lat = models.FloatField()
    lng = models.FloatField()
    datetime = models.DateTimeField()


class Ride(models.Model):
    start_coords = models.ForeignKey(
        Coords, on_delete=models.CASCADE, related_name='start_coords'
    )
    end_coords = models.ForeignKey(
        Coords, on_delete=models.CASCADE, related_name='end_coords'
    )

    number_of_free_seats = models.IntegerField()
    car_brand = models.CharField(max_length=128)
    car_model = models.CharField(max_length=128)
    car_color = models.CharField(max_length=32, blank=True)
    car_register_number = models.CharField(max_length=16, blank=True)

    owner = models.ForeignKey(User, on_delete=models.CASCADE)