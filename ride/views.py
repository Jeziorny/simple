from rest_framework import generics, permissions

from ride.serializers import (
    PostRideSerializer, ListRideSerializer, UpdateRideSerializer,
    DestroyRideSerializer,
)
from ride.models import Ride
from ride.permissions import IsOwner


class RideCreateAPIView(generics.CreateAPIView):
    serializer_class = PostRideSerializer
    queryset = Ride.objects.all()
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class RideListAPIView(generics.ListAPIView):
    serializer_class = ListRideSerializer
    queryset = Ride.objects.all()
    permission_classes = [permissions.IsAuthenticated]


class RideUpdateAPIView(generics.UpdateAPIView):
    serializer_class = UpdateRideSerializer
    queryset = Ride.objects.all()
    permission_classes = [permissions.IsAuthenticated]


class RideDestroyAPIView(generics.DestroyAPIView):
    serializer_class = DestroyRideSerializer
    queryset = Ride.objects.all()
    permission_classes = [IsOwner]

